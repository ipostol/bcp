const axios = require("axios");
const { TelegramClient } = require("telegram");
const { StringSession } = require("telegram/sessions");

const session =
  "1AgAOMTQ5LjE1NC4xNjcuNDEBuzPQEkv1rrmMBhlWfKPcNqp8u97A3Sg1FMfWzQ9cMgVBU453KP/4cUuHRILStPiw+ACkkhDitkgTLTH2d3a8lZ4Hm+b91eNhjR1rxkEZO4FlnZNs0HaAZFRDKiYiklep+vwRMd1ADTXntqgXyxRFC/HPWW9ncZVJAWdVqcQnViUNZUQx4abjdX2UwAO7L6CZ+XYLIJNXG/ux/zySYrCOASbAqgqmjj5ZYsk/6O/9SunzX58vtKBatBzvwa02hYTYbsB+HBd4WSLz+vkNeZF00F9n85aS720Kd9plkyvV7ybQ6jCGwc/M2S+QWUwjiWwyNpOZx7LXNttg42yCKBY56qg=";
const apiId = 17517572;
const apiHash = "e3c520255a896e7019d3bfd9dc4d1c3d";

let price = 1.007;

const sleep = () =>
  new Promise((resolve) =>
    setTimeout(() => {
      resolve();
    }, 20000)
  );

let started = 0;
let response = "";
const logs = [];

let clientStarted = false;

const startClient = async () => {
  if (clientStarted) {
    return clientStarted;
  }

  const stringSession = new StringSession(session);
  const client = new TelegramClient(stringSession, apiId, apiHash, {
    connectionRetries: 1,
  });

  if (stringSession._serverAddress === "venus.web.telegram.org") {
    stringSession._serverAddress = "149.154.167.41";
    stringSession.serverAddress = "149.154.167.41";
  } else if (stringSession._serverAddress === "vesta.web.telegram.org") {
    stringSession._serverAddress = "149.154.167.91";
    stringSession.serverAddress = "149.154.167.91";
  }

  client.setLogLevel("warn");

  await client.start({
    phoneNumber: async () => Promise.reject("Invalid sessions number"),
    password: async () => Promise.reject("Invalid sessions password"),
    phoneCode: async () => Promise.reject("Invalid sessions phoneCode"),
    onError: async (e) => {
      console.log("Invalid sessions onError", phone, e);
      return await Promise.resolve(true); // should we stop
    },
  });

  clientStarted = client;

  return client;
};

const start = async () => {
  const action = async () => {
    logs.push({ date: new Date().toISOString(), started });
    started++;

    const {
      data: { data },
    } = await axios.post(
      "https://p2p.binance.com/bapi/c2c/v2/friendly/c2c/adv/search",
      {
        proMerchantAds: false,
        page: 1,
        rows: 10,
        payTypes: ["Revolut"],
        countries: [],
        publisherType: null,
        asset: "USDT",
        fiat: "USD",
        tradeType: "BUY",
      }
    );

    const okPrice = data[0];

    // const okPrice = data.find((item) => {
    //   return (
    //     Number(item.adv.price) < price &&
    //     Number(item.adv.maxSingleTransAmount) > 300
    //   );
    // });

    response = JSON.stringify(data);

    // if (okPrice) {
    console.log("start send", Date.now());
    try {
      const client = await startClient();
      await client.sendMessage("@ipostol", {
        message: `Find best price ${okPrice.adv.price}`,
      });
    } catch (e) {
      console.log(e, "error");
    }
    console.log("end send", Date.now());
    // } else {
    //   console.log("not found ok price");
    // }

    setTimeout(action, 5000);
  };

  action();
  interval = true;
};

const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send(`Hello World! ${started}`);
});

app.get("/setprice", (req, res) => {
  if (req.query.price) {
    price = Number(req.query.price);
  }
  res.send("Ok, price:" + price);
});

app.get("/logs", (req, res) =>
  res.send(JSON.stringify({ date: Date.now(), logs }))
);

app.get("/response", (req, res) => res.send(response));

const port = process.env.PORT || 9001;

app.listen(port, () => {
  console.log(`Listening to port ${port}`);
});

start();
